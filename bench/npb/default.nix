{ stdenv, lib, fetchurl, gfortran }:

stdenv.mkDerivation rec {
  pname = "npb";
  version = "4.2";

  src = fetchurl {
    url = "https://www.nas.nasa.gov/assets/npb/NPB3.${version}.tar.gz";
    sha256 = "sha256-+741tZRgainW9v3GQRK8mIvO5lxpvkobAS+E3u1nmuw=";
  };

  buildInputs = [ gfortran ];

  configurePhase = ''
    cp -a NPB3.4-OMP/* .
    cp config/make.def.template config/make.def
    #
    # (except that no class E for IS and UA).
    for bench in ft mg sp lu bt is ep cg ua; do
       for class in S W A B C D; do
           echo $bench $class > config/suite.def
       done
    done
  '';

  buildPhase = ''
    make suite
  '';

  installPhase = ''
    mkdir -p $out/bin
    mv bin $out/
  '';

  meta = with lib; {
    description = "NAS Parallel Benchmarks";
    longDescription = ''
      The NAS Parallel Benchmarks (NPB) are a small set of programs designed to help evaluate the performance of parallel supercomputers. 
    '';
    homepage = "https://www.nas.nasa.gov/software/npb.html";
    license = licenses.gpl2Plus;
    platforms = platforms.all;
    broken = false;
  };
}
