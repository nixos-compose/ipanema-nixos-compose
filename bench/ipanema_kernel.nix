ipanema_name: ipanema_kernel:
{ pkgs, ... }: {
  nodes = {
    ${ipanema_name} = { pkgs, lib, ... }: {
      
      imports = [ ((import ./common_config.nix) { inherit pkgs; }) ];
      
      boot.kernelPackages = let
        linux_ipanema = { fetchgit, buildLinux, ... }@args:
          buildLinux (args // rec {
            version = ipanema_kernel.version;
            modDirVersion = version;
            src = fetchgit ipanema_kernel.src;
            kernelPatches = [ ];
            #extraConfig = ''
            #  INTEL_SGX y
            #'';
          } // (args.argsOverride or { }));
        ipanema_linux_kernel = pkgs.callPackage linux_ipanema { };
      in pkgs.recurseIntoAttrs (pkgs.linuxPackagesFor ipanema_linux_kernel);
    };
  };
  
  testScript = ''
    ${ipanema_name}.succeed("true")
  '';
}
