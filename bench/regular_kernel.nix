regular_kernel:
{ pkgs, ... }:
let rt-tests = pkgs.callPackage ./rt-tests { }; in
{
  nodes = {
    ${regular_kernel} = { pkgs, lib, ... }: {
      boot.kernelPackages = pkgs.linuxKernel.packages.${regular_kernel};

      environment.systemPackages = [ rt-tests ];
    };
  };
  testScript = ''
    ${regular_kernel}.succeed("true")
  '';
}
