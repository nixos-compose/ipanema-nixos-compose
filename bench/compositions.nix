let
  ipanema_kernels = {
    ipanema_5_4 = {
      version = "5.4.0";
      src = {
        url = "https://gitlab.inria.fr/ipanema-public/ipanema-kernel.git";
        rev = "73efe6cbd7d7450830ea36d55dbd2baabebe7eaf";
        sha256 = "sha256-gUDpkOUyezJs1LrlCqJq8Ky74bQPVqBMSgp+J1d4QBg=";
      };
    };
  };
  regular_kernels = [ "linux_4_19" "linux_5_4" ];
in builtins.listToAttrs (map (n: {
  name = n;
  value = (import ./regular_kernel.nix) n;
}) regular_kernels)
// builtins.mapAttrs (n: v: (import ./ipanema_kernel.nix) n v) ipanema_kernels
